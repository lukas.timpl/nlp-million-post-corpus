# Author Attribution: DerStandard Forum Writing Style

The main aim of this project is to identify the authors of posts to a newspaper forum. This should be achieved through the incorporation of multiple factors, such as writing style and content, but also meta information such as the post date and the article to which the post was written. In that, the setting differentiates itself from the classical authorship attribution setting, where no meta information is used. 

The dataset we used for this project was the One Million Posts Corpus, provided by: https://ofai.github.io/million-post-corpus/

A detailed project report on the setting, results and discussion can be found [here](report/report.pdf).

## Contributors

* Lukas Timpl, [lukas.timpl@student.tugraz.at](mailto:lukas.timpl@student.tugraz.at)
* Patrick Deutschmann, [patrick.deutschmann@student.tugraz.at](mailto:patrick.deutschmann@student.tugraz.at)